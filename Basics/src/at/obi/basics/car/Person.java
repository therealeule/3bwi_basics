package at.obi.basics.car;

import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstname;
	private String lastname;
	private int age;
	private String country;
	
	private List<Car> cars;

	public Person(String firstname, String lastname, int age, String country) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
		this.country = country;
		this.cars = new ArrayList<>();
		
		
	}
	
	
	
	
	public void addcar(Car c) {
		this.cars.add(c);
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public int getAge() {
		return age;
	}

	public String getCountry() {
		return country;
	}

	public List<Car> getCars() {
		return cars;
	}
	int getValueOfcars() {
        int carSum = 0;
        for (Car car : cars) {
            carSum++;
        }
        return carSum;
    }
}
