package at.obi.basics.car;

public class CarStarter {

	public static void main(String[] args) {
		
		Producer p1 = new Producer ("Ferrari", "Italien", 0.10);
		Engine e1 = new Engine("Benzin", 720, 500000, 6);
		Car c1 = new Car ("Black", 200, 202493.99, 7.1, p1, e1);
		
		Person pe1 = new Person ("Niklas", "Mischi", 16, "Austria");

		double consumtion; 
		if (e1.getKilometers() > 50000) {
			 consumtion = c1.getBaseConsumption() * 1.098;
		}
		else {
				consumtion = c1.getBaseConsumption();
		}
		
		double prize = c1.getBasePrice() * (1 - p1.getDiscount());
		
		pe1.addcar(c1);
		
		for (Car car: pe1.getCars()) {
			System.out.println(car.getColor());
			System.out.println(p1.getName());
		}
		
		System.out.println(pe1.getValueOfcars());
		System.out.println("Firstname: " + pe1.getFirstname() + " Lastname: " + pe1.getLastname()  + " Age: " + pe1.getAge() + " Country: " + pe1.getCountry());
		
//		System.out.println("Name: " + p1.getName() + " Country of Origin: " + p1.getCountryoforigin());
//		System.out.println("Type: " + e1.getType() + " Ps: " + e1.getPs() + " Color: " + c1.getColor() + " Max Speed: " + c1.getMaxSpeed());
//		
	System.out.println("Prize: " + prize);
	System.out.println("Consumtion: " + consumtion);
	}
	
}
