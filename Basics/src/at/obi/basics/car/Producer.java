package at.obi.basics.car;

public class Producer {
	private String name;
	private String countryoforigin;
	private double discount;
	
	
	public Producer(String name, String countryoforigin, double discount) {
		super();
		this.name = name;
		this.countryoforigin = countryoforigin;
		this.discount = discount;
	}


	public double getDiscount() {
		return discount;
	}


	public String getName() {
		return name;
	}


	public String getCountryoforigin() {
		return countryoforigin;
	}


	
	
	
}
