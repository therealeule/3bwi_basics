package at.obi.basics.car;

public class Engine {
	private String type;
	private double ps;
	private int kilometers;
	private double gasConsumption;
	
	public Engine(String type, double ps, int kilometers, double gasConsumption) {
		super();
		
//		if (kilometers > 50000) {
//			gasConsumption =  0.098;
//		}
		
		this.type = type;
		this.ps = ps;
		this.kilometers = kilometers;
		this.gasConsumption = gasConsumption;
		
		
		
		
		
	}

	public int getKilometers() {
		return kilometers;
	}

	public double getGasConsumption() {
		return gasConsumption;
	}

	public String getType() {
		return type;
	}

	public double getPs() {
		return ps;
	}

	

	
	
	
}
