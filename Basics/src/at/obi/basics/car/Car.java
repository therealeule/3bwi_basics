package at.obi.basics.car;

public class Car {
	private String color;
	private double maxSpeed;
	private double basePrice;
	private double baseConsumption;
	
	private Producer producer;
	private Engine engine;
	
	
	public Car(String color, double maxSpeed, double basePrice, double baseConsumption,
			at.obi.basics.car.Producer producer, at.obi.basics.car.Engine engine) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basePrice = basePrice;
		this.baseConsumption = baseConsumption;
		this.producer = producer;
		this.engine = engine;
	}


	public double getBasePrice() {
		return basePrice;
	}


	public void setBaseConsumption(double baseConsumption) {
		this.baseConsumption = baseConsumption;
	}


	public double getBaseConsumption() {
		return baseConsumption;
	}



	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public double getMaxSpeed() {
		return maxSpeed;
	}




	public void sayHello() {
		System.out.println("Color: " + this.color + "Max Speed: " + this.maxSpeed + "Base Price: " + this.basePrice + "Base Consumption : " + this.baseConsumption + "Producer: " + this.producer + "Engine: " + this.engine);
	}
	
	
	
	
}
