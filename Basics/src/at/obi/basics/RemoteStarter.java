package at.obi.basics;

public class RemoteStarter {

	public static void main(String[] args) {
		
		Battery b1 = new Battery (45, "AA");
		Remote r1 = new Remote(400, 40, 20, 30, false, b1);

		System.out.println(b1.getBatteryStatus());
	}
	
}
