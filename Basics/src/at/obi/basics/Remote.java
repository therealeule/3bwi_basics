package at.obi.basics;

public class Remote {
	private double weight;
	private double length;
	private double width;
	private double height;
	private boolean ison = false;
	
	private Battery battery;

	public Remote(double weight, double length, double width, double height, boolean turn, Battery battery) {
		super();
		this.weight = weight;
		this.length = length;
		this.width = width;
		this.height = height;
		this.ison = turn;
		this.battery = battery;
		

	}

	public void turnon() {
		System.out.println("I am turend on now");
		this.ison = true;
	}

	public void turnoff() {
		System.out.println("I am turned off now");
		this.ison = false;
	}

	public void sayHello() {
		System.out.println("Weight: " + this.weight + "length: " + this.length + "width: " + this.width + "height: " + this.height + "On: " + this.ison);
	}

//	public static void main(String[] args) {
//		Remote r1 = new Remote(400, 40, 20, 30, false, battery);
//		Remote r2 = new Remote(600, 30, 30, 30, false, battery);
//
//		r1.turnon();
//		r2.turnoff();
//		
//		r1.sayHello();
//		r2.sayHello();
//	}
}
